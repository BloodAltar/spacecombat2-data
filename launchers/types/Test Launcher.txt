[Configuration]
Name=Test Launcher
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Small Shielded Kinetic Round=true
Small Missile=true
Small Hybrid Charge=true
Small Cannon Round=true
Small Plasma Container=true
Small Burst Round=true
Small Particle Canister=true

[Slots/TestSlot]
Test Upgrades=true

[LauncherConfiguration/Targeting]
HasTargeting=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=30

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=10
TimeBetweenBurstShots=0.05

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=500
ReloadTime=10

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=10
PG=100
Slot=Tiny Weapon
Classes.1=Drone
Classes.2=Fighter

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.01
MaximumSpread=0.015

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(3500, 0, 0)