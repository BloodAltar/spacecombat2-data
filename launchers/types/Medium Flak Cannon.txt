[Configuration]
Name=Medium Flak Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Medium Burst Round=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=18

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=6
TimeBetweenBurstShots=0.25

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=48
ReloadTime=15

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=100
PG=1000
Slot=Medium Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.05
MaximumSpread=0.085

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(13000, 0, 0)