[Configuration]
Name=Small Swarm Missile Launcher
Description=Unleash a swarm of missiles upon your enemies!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Tiny Missile=true

[LauncherConfiguration/Targeting]
HasTargeting=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=10

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=10
TimeBetweenBurstShots=0.05

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=50
ReloadTime=60

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=100
PG=700
Slot=Small Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.01
MaximumSpread=0.015

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=5
TemperaturePerShot=2
CoolingRate=1

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(3500, 0, 0)