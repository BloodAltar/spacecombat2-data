[Configuration]
Name=Medium Torpedo Launcher
Description=Unleash a torpedo upon your enemies!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Medium Torpedo=true

[LauncherConfiguration/Targeting]
HasTargeting=false

[LauncherConfiguration/FireRate]
ShotsPerMinute=20

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=5
ReloadTime=15

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=10000
CapacitorFillRate=200
CapacitorPerShot=1000

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=20
PG=100
Slot=Medium Weapon

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(3500, 0, 0)