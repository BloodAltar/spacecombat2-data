[Configuration]
Name=Medium Beam
Description=SCIENCE!
Class=Launcher_Beam

[Configuration/CompatibleProjectileFamilies]
Medium Laser Crystal=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=120

[LauncherConfiguration/Magazine]
HasMagazine=false

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=false

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=10
PG=100
Slot=Medium Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=false

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=80
TemperaturePerShot=1.4
CoolingRate=10