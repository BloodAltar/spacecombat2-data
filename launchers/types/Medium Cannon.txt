[Configuration]
Name=Medium Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Medium Cannon Round=true
Medium Shielded Kinetic Round=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=15

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=60
ReloadTime=20

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=100
ResourcesPerReload.Energy=5000

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=100
PG=1000
Slot=Medium Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.00015
MaximumSpread=0.00185

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(8000, 0, 0)