

[Environment/Shakuras]
Location=8566.000000 -7744.000000 -9218.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=5120

[EnvironmentResources/Shakuras]
Carbon Dioxide=14917115
Hydrogen=67127019
Oxygen=596684615
Nitrogen=67127019
Methane=30893434
Carbon Monoxide=39720129
Water=242734126


[Environment/Dunomane]
Location=9092.000000 9314.000000 -8874.000000
Type=planet
Gravity=1
Temperature=325
Atmosphere=1
Radius=4096

[EnvironmentResources/Dunomane]
Carbon Dioxide=57281723
Hydrogen=95469538
Oxygen=152751261
Nitrogen=76375630
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/Ninurta]
Location=-8192.919922 8959.370117 7097.500000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=3122

[EnvironmentResources/Ninurta]
Carbon Dioxide=42359555
Hydrogen=33820004
Oxygen=8370451
Nitrogen=84550011
Methane=7004142
Carbon Monoxide=9005326
Water=55032551


[Environment/Vimana]
Location=-10696.000000 -6888.999512 -6131.000000
Type=planet
Gravity=0.9
Temperature=700
Atmosphere=1.2
Radius=3122

[EnvironmentResources/Vimana]
Carbon Dioxide=84550011
Hydrogen=40584005
Oxygen=1691000
Nitrogen=42275005
Methane=7004142
Carbon Monoxide=9005326
Water=55032551


[Environment/Star]
Location=13422.000000 13253.999023 13308.000000
Type=star
Gravity=0
Temperature=2000
Atmosphere=0
Radius=3000

[EnvironmentResources/Star]


[Environment/Station 457]
Location=9414.000000 9882.000000 391.999969
Type=planet
Gravity=0.3
Temperature=288
Atmosphere=1
Radius=2600

[EnvironmentResources/Station 457]
Hydrogen=14650629
Oxygen=68369602
Nitrogen=14650629
Methane=4045538
Carbon Monoxide=5201406
Water=31786374


[Environment/Chondradirk]
Location=5854.000000 -3906.000244 6512.000000
Type=planet
Gravity=0.5
Temperature=260
Atmosphere=0.5
Radius=1792

[EnvironmentResources/Chondradirk]
Carbon Dioxide=3197856
Hydrogen=21105853
Oxygen=639571
Nitrogen=7035284
Methane=1324555
Carbon Monoxide=1703000
Water=10407225


[Environment/Spawn Room]
Location=59.999996 -8608.000000 10624.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=600

[EnvironmentResources/Spawn Room]
Oxygen=1200324
Methane=49717
Carbon Monoxide=63922
Water=390638
