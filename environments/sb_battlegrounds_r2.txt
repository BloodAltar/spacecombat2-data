[Environment/Lag Bunker Cluster]
Location=-10915.000000 15115.999023 -13943.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=8804

[EnvironmentResources/Lag Bunker Cluster]
Carbon Dioxide=379214619
Hydrogen=1137643859
Oxygen=1137643859
Nitrogen=1137643859
Methane=157071144
Carbon Monoxide=201948614
Water=1234130419


[Environment/Aura]
Location=-7648.000000 7424.000000 1536.000000
Type=planet
Gravity=1.5
Temperature=270
Atmosphere=4
Radius=4300

[EnvironmentResources/Aura]
Carbon Dioxide=41973379
Hydrogen=353460041
Oxygen=19882127
Nitrogen=22091252
Methane=18300445
Carbon Monoxide=23529144
Water=143789218


[Environment/Keltash]
Location=8976.000000 9194.000000 -7823.000000
Type=planet
Gravity=1
Temperature=600
Atmosphere=2
Radius=4096

[EnvironmentResources/Keltash]
Carbon Dioxide=152751261
Hydrogen=114563446
Nitrogen=114563446
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/Cydonia]
Location=9096.000000 9856.000000 9456.000000
Type=planet
Gravity=1
Temperature=290
Atmosphere=1
Radius=3860

[EnvironmentResources/Cydonia]
Carbon Dioxide=3196000
Hydrogen=15980004
Oxygen=95880025
Nitrogen=217328057
Methane=13237873
Carbon Monoxide=17020122
Water=104011861


[Environment/Rhodon]
Location=-9856.000000 -10112.000000 -11246.000000
Type=planet
Gravity=1
Temperature=300
Atmosphere=1
Radius=3860

[EnvironmentResources/Rhodon]
Carbon Dioxide=22372005
Oxygen=79900020
Nitrogen=217328057
Methane=13237873
Carbon Monoxide=17020122
Water=104011861


[Environment/Zarkol (safe zone)]
Location=-11568.000000 -12408.000000 9680.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=3700

[EnvironmentResources/Zarkol (safe zone)]
Carbon Dioxide=14074084
Oxygen=267407601
Methane=11659004
Carbon Monoxide=14990148
Water=91606465


[Environment/Kastiel]
Location=10424.000000 -10120.000000 -4912.000000
Type=planet
Gravity=0.7
Temperature=160
Atmosphere=0.7
Radius=2840

[EnvironmentResources/Kastiel]
Carbon Dioxide=50916668
Hydrogen=38187501
Nitrogen=38187501
Methane=5272436
Carbon Monoxide=6778846
Water=41426283


[Environment/Star]
Location=5337.560059 -13977.200195 12333.000000
Type=star
Gravity=0
Temperature=500
Atmosphere=0
Radius=2000

[EnvironmentResources/Star]


[Environment/Lava asteroid Belt 2]
Location=2984.330322 -8552.259766 11427.499023
Type=planet
Gravity=0.08
Temperature=800
Atmosphere=0.1
Radius=2000

[EnvironmentResources/Lava asteroid Belt 2]
Carbon Dioxide=8891293
Hydrogen=35565174
Methane=1841392
Carbon Monoxide=2367504
Water=14468081


[Environment/Lava asteroid Belt 1]
Location=-69.374397 -10500.600586 11427.499023
Type=planet
Gravity=0.08
Temperature=800
Atmosphere=0.1
Radius=1800

[EnvironmentResources/Lava asteroid Belt 1]
Carbon Dioxide=6481753
Hydrogen=25927012
Methane=1342374
Carbon Monoxide=1725910
Water=10547231


[Environment/Nakvir]
Location=384.000000 9984.000000 3135.999756
Type=planet
Gravity=0.16
Temperature=75
Atmosphere=0.1
Radius=1350

[EnvironmentResources/Nakvir]
Carbon Dioxide=683622
Hydrogen=6152601
Nitrogen=6836223
Methane=566314
Carbon Monoxide=728118
Water=4449613


[Environment/Freedonia]
Location=-10752.000000 -512.000000 6656.000000
Type=planet
Gravity=0.3
Temperature=205
Atmosphere=0.3
Radius=1125

[EnvironmentResources/Freedonia]
Carbon Dioxide=474737
Hydrogen=3560533
Oxygen=316491
Nitrogen=3560533
Methane=327728
Carbon Monoxide=421364
Water=2575007


[Environment/Planetoid GAX-738]
Location=-14336.000000 -768.000000 13952.000000
Type=planet
Gravity=0.09
Temperature=180
Atmosphere=0.2
Radius=900

[EnvironmentResources/Planetoid GAX-738]
Carbon Dioxide=202554
Hydrogen=2228102
Nitrogen=1620438
Methane=167796
Carbon Monoxide=215738
Water=1318403


[Environment/Spawn]
Location=-15727.999023 -16116.000000 -16251.999023
Type=planet
Gravity=1
Temperature=290
Atmosphere=1
Radius=800

[EnvironmentResources/Spawn]
Oxygen=2845213
Methane=117849
Carbon Monoxide=151520
Water=925957
