[Configuration]
Name=Small Depleted Uranium Particle Canister
OwnerID=NULL
ProjectileType=Small Particle Canister

[Components/Payload]
Component=Small Depleted Uranium Particle Canister

[Components/Body]
Component=Small Particle Blaster Effect
Configuration.Color=Color(0,220,0)

[Components/Trail]
Component=Small Trail
Configuration.Color=Color(0,220,0)
Configuration.Length=400
Configuration.Width=75

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_railgun