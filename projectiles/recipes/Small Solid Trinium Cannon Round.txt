[Configuration]
Name=Small Solid Trinium Cannon Round
OwnerID=NULL
ProjectileType=Small Cannon Round

[Components/Payload]
Component=Small Solid Trinium Mass

[Components/Body]
Component=Small Projectile Body

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_artillery