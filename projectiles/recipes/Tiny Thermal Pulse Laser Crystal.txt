[Configuration]
Name=Tiny Thermal Pulse Laser Crystal
OwnerID=NULL
ProjectileType=Tiny Pulse Laser Crystal

[Components/Attunement Crystal]
Component=Tiny Thermal Pulse Attunement Crystal

[Components/Beam Modulator]
Component=Tiny Pulse Beam Effect
Configuration.Color=Color(0, 180, 255)

[Components/Hit Effect]
Component=Tiny Legacy Hit Effect
Configuration.HitEffect=impact_autocannon