[Configuration]
Name=Depleted Uranium Mini MAC Round
OwnerID=NULL
ProjectileType=Mini MAC Round

[Components/Payload]
Component=Mini MAC Depleted Uranium Mass

[Components/Body]
Component=Mini MAC Effect

[Components/Hit Effect]
Component=X-Large Legacy Hit Effect
Configuration.HitEffect=impact_artillery