[Configuration]
Name=Medium Solid Nubium Hybrid Charge
OwnerID=NULL
ProjectileType=Medium Hybrid Charge

[Components/Payload]
Component=Medium Solid Nubium Mass

[Components/Body]
Component=Medium Railgun Effect
Configuration.Color=Color(160,255,200)

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_railgun