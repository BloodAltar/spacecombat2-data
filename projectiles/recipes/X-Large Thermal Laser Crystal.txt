[Configuration]
Name=X-Large Thermal Laser Crystal
OwnerID=NULL
ProjectileType=X-Large Laser Crystal

[Components/Attunement Crystal]
Component=X-Large Thermal Attunement Crystal

[Components/Beam Modulator]
Component=X-Large Beam Effect
Configuration.Color=Color(180, 0, 180)

[Components/Hit Effect]
Component=X-Large Legacy Hit Effect
Configuration.HitEffect=impact_autocannon