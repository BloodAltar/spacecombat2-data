[Configuration]
Name=X-Large Tachyon Particle Canister
OwnerID=NULL
ProjectileType=X-Large Tachyon Beam

[Components/Focus]
Component=X-Large Tachyon Particle Canister

[Components/Body]
Component=X-Large Tachyon Beam Effect

[Components/Hit Effect]
Component=X-Large Legacy Hit Effect
Configuration.HitEffect=impact_artillery