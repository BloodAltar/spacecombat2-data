[Configuration]
Name=Large Trinium Hybrid Charge
OwnerID=NULL
ProjectileType=Large Hybrid Charge

[Components/Payload]
Component=Large Solid Trinium Mass

[Components/Body]
Component=Large Railgun Effect
Configuration.Color=Color(100,160,255)

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_railgun