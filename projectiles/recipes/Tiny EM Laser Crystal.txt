[Configuration]
Name=Tiny EM Laser Crystal
OwnerID=NULL
ProjectileType=Tiny Laser Crystal

[Components/Attunement Crystal]
Component=Tiny EM Attunement Crystal

[Components/Beam Modulator]
Component=Tiny Beam Effect
Configuration.Color=Color(0, 80, 255)

[Components/Hit Effect]
Component=Tiny Legacy Hit Effect
Configuration.HitEffect=impact_autocannon