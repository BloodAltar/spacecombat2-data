[Configuration]
Name=Small Pulse Round
OwnerID=NULL
ProjectileType=Small Shielded Kinetic Round

[Components/Payload]
Component=Small Solid Nubium Mass

[Components/Field]
Component=Small EM Plasma Field

[Components/Body]
Component=Small Pulse Effect
Configuration.Color=Color(200,100,255)

[Components/Trail]
Component=Small Trail
Configuration.Color=Color(200,100,255)
Configuration.Length=300
Configuration.Width=100

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_pulsecannon