[Configuration]
Name=Small EM Pulse Laser Crystal
OwnerID=NULL
ProjectileType=Small Pulse Laser Crystal

[Components/Attunement Crystal]
Component=Small EM Pulse Attunement Crystal

[Components/Beam Modulator]
Component=Small Pulse Beam Effect
Configuration.Color=Color(120, 200, 0)

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_autocannon