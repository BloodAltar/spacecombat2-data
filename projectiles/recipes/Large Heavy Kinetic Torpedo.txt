[Configuration]
Name=Large Heavy Kinetic Torpedo
OwnerID=NULL
ProjectileType=Large Torpedo

[Components/Payload]
Component=X-Large Solid Trinium Mass

[Components/Body]
Component=Large Missile Body
Configuration.Model=models/props_phx/torpedo.mdl

[Components/Thrusters]
Component=Large Thruster

[Components/Trail]
Component=Large Missile Trail

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_autocannon