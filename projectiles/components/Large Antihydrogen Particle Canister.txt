[Configuration]
Name=Large Antihydrogen Particle Canister
Description=A Large canister filled with antihydrogen particles
Class=DamageComponent
Family=Large Particle Canister
Mass=0.5

[ClassData]
Velocity=15000
Falloff=500
Damage.EM=30000
Damage.EXP=200000
Damage.KIN=40000
Damage.THERM=30000
Damage.ANTIMATTER=80000

[ResourceCost]
Anti-Hydrogen Particle Canister=25