[Configuration]
Name=Targeting Component
Description=A component that lets the projectile target entities.
Class=TargetingComponent
Family=TargetingComponent
Hidden=true