[Configuration]
Name=Medium Detonation Timer
Description=A timer that blows up the projectile after some time.
Class=TimedEventComponent
Family=Medium Detonation Timer

[ClassData]
EventName=Detonate
Delay=3
Networked=false

[ToolOptions/Delay]
Type=Number
Mode=Slider
Min=1
Max=5
Default=3