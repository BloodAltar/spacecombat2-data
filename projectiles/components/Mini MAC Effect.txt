[Configuration]
Name=Mini MAC Effect
Description=MAC effect visual.
Class=MACEffectComponent
Family=Mini MAC Body

[ClassData]
Scale=0.75
Color=Color(100, 20, 20)

[ToolOptions/Color]
Type=Color
Default=Color(100, 20, 20)