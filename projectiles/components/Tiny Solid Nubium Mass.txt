[Configuration]
Name=Tiny Solid Nubium Mass
Description=A tiny, heavy object made out of solid nubium.
Class=KineticDamageComponent
Family=Tiny Mass
Mass=1