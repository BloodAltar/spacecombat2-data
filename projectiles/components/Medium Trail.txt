[Configuration]
Name=Medium Trail
Description=A trail visual.
Class=TrailComponent
Family=Medium Trail

[ClassData]
NumSegments=5
Material=trails/laser.vmt
Length=750
Width=100
Color=Color(255,255,255)

[ToolOptions/Color]
Type=Color
Default=Color(255,255,255)

[ToolOptions/NumSegments]
Type=Number
Mode=Slider
Min=1
Max=10
Default=5

[ToolOptions/Length]
Type=Number
Mode=Slider
Min=300
Max=1200
Default=750

[ToolOptions/Width]
Type=Number
Mode=Slider
Min=10
Max=1000
Default=100

[ToolOptions/Material]
Type=String
Mode=ComboBox
Options.1=trails/laser.vmt
Options.2=trails/plasma.vmt
Options.3=trails/smoke.vmt
Options.4=trails/physbeam.vmt
Default=trails/laser.vmt