[Configuration]
Name=X-Large Thermal Warhead
Description=A X-Large warhead that does primarily thermal damage.
Class=ExplosiveComponent
Family=X-Large Warhead
Mass=1

[ClassData]
Radius=500
Velocity=15000
Falloff=1000
Damage.EM=100000
Damage.EXP=100000
Damage.THERM=550000