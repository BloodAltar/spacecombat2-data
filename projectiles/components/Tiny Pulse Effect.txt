[Configuration]
Name=Tiny Pulse Effect
Description=A pulse effect visual.
Class=PulseCannonEffectComponent
Family=Tiny Shielded Round

[ClassData]
Scale=0.15
Color=Color(100, 100, 255)

[ToolOptions/Color]
Type=Color
Default=Color(100, 100, 255)