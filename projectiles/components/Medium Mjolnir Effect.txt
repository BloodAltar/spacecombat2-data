[Configuration]
Name=Medium Mjolnir Effect
Description=A mjolnir effect visual.
Class=MjolnirEffectComponent
Family=Medium Missiles

[ClassData]
Scale=1.0
Color=Color(255, 150, 80)

[ToolOptions/Color]
Type=Color
Default=Color(255, 150, 80)