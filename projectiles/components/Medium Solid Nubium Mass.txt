[Configuration]
Name=Medium Solid Nubium Mass
Description=A medium, heavy object made out of solid nubium.
Class=KineticDamageComponent
Family=Medium Mass
Mass=20