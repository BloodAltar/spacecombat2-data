[Configuration]
Name=Mini Tachyon Beam
Description=A tachyon beam made from tachyon particles. beams go brrrrrr

[Slots/Focus]
Mini Tachyon Beam Canister=true

[Slots/Body]
Mini Tachyon Beam=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Beam Collision Component

[MandatoryComponents/Movement Component]
Component=Localized Movement Component