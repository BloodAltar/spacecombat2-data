[Configuration]
Name=Tiny Missile
Description=A tiny missile that can equip homing thrusters and various warheads.

[Slots/Payload]
Tiny Warhead=true
Tiny Mass=true

[Slots/Body]
Tiny Missiles=true

[Slots/Thrusters]
Tiny Homing Thrusters=true

[Slots/Trail]
Tiny Trail=true

[Slots/Hit Effect]
Tiny Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component

[MandatoryComponents/Targeting Component]
Component=Targeting Component