[Configuration]
Name=Medium Plasma Container
Description=A plasma container that can be fired in many forms.

[Slots/Payload]
Medium Plasma=true

[Slots/Body]
Medium Plasma Body=true

[Slots/Hit Effect]
Medium Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component