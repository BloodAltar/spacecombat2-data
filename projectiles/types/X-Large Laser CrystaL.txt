[Configuration]
Name=X-Large Laser Crystal
Description=A X-Large crystal used to change the damage type and color of lasers.

[Slots/Attunement Crystal]
X-Large Laser Crystal=true

[Slots/Beam Modulator]
X-Large Beam=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Beam Collision Component

[MandatoryComponents/Movement Component]
Component=Localized Movement Component