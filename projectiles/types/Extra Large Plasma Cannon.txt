[Configuration]
Name=X-Large Plasma Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
X-Large Plasma Container=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=20

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=40
ReloadTime=10

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=10
PG=100
Slot=X-Large Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.0005
MaximumSpread=0.00085

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(5000, 0, 0)