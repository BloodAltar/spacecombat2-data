[Configuration]
Name=Tiny Burst Round
Description=A tiny cannon round that can equip a self destruct timer and various warheads.

[Slots/Payload]
Tiny Warhead=true

[Slots/Body]
Tiny Projectiles=true

[Slots/Timer]
Tiny Detonation Timer=true

[Slots/Trail]
Tiny Trail=true

[Slots/Hit Effect]
Tiny Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component