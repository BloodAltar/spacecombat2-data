[Configuration]
Name=Small Missile
Description=A small missile that can equip homing thrusters and various warheads.

[Slots/Payload]
Small Warhead=true
Small Mass=true

[Slots/Body]
Small Missiles=true

[Slots/Thrusters]
Small Homing Thrusters=true

[Slots/Trail]
Small Trail=true

[Slots/Hit Effect]
Small Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component

[MandatoryComponents/Targeting Component]
Component=Targeting Component