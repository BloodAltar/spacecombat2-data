[Configuration]
Name=Mini MAC Round
Description=A Mini MAC round that can be made out of various materials. 

[Slots/Payload]
Mini MAC Mass=true

[Slots/Body]
Mini MAC Body=true

[Slots/Trail]
Medium Trail=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component