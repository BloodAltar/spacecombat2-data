[Configuration]
Name=Large Burst Round
Description=A Large cannon round that can equip a self destruct timer and various warheads.

[Slots/Payload]
Large Warhead=true

[Slots/Body]
Large Projectiles=true

[Slots/Timer]
Large Detonation Timer=true

[Slots/Trail]
Large Trail=true

[Slots/Hit Effect]
Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component