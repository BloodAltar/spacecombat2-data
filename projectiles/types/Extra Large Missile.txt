[Configuration]
Name=X-Large Missile
Description=A X-Large missile that can equip homing thrusters and various warheads.

[Slots/Payload]
X-Large Warhead=true
X-Large Mass=true

[Slots/Body]
X-Large Missiles=true

[Slots/Thrusters]
X-Large Homing Thrusters=true

[Slots/Trail]
X-Large Trail=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component

[MandatoryComponents/Targeting Component]
Component=Targeting Component