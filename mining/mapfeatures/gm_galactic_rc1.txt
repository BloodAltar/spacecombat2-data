[Ship Asteroid Belt]
Type=Belt
Pos1=10924,-3641,160
Pos2=10924,9008,160
MaxCount=30
Variance=3500
Ignore Atmospheres=0

[Village Mineral Field]
Type=Crystal Field
Pos=-7239,2586.5,7680
MaxCount=150
ScanHeight=1500
RespawnRate=240
MinInitialSpawn=80
FieldHealth=500000
CrystalType=2
Ignore Atmospheres=0