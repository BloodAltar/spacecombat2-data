[Asteroid Belt P1]
Type=Belt
Pos1=-1500,-14500,11500
Pos2=4000,-4000,11700
MaxCount=25
Variance=1500
Ignore Atmospheres=1

[Asteroid Belt P2]
Type=Belt
Pos1=4000,-4000,11700
Pos2=14200,-13000,12100
MaxCount=25
Variance=1500
Ignore Atmospheres=1

[Ice Planet Ring]
Type=Ring
Pos=-7648,7424,1536
Radius=8280
InnerRadius=5280
Inclination=-4.49
Azimuth=0
Thickness=100
MaxCount=100
Ignore Atmospheres=0

[Keltash Caldera Crystals]
Type=Crystal Field
Pos=9039,8803,-8032
Normal=0.2,0.8,0.5
MaxCount=150
ScanHeight=150
RespawnRate=240
MinInitialSpawn=80
FieldHealth=500000
CrystalType=2
Ignore Atmospheres=0