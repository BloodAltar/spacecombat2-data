[Large Shelf Rock]
Model=models/props_foliage/rock_coast02b.mdl
PossibleElements=Veldspar:500:1,Uranium:50:0.5,Carbon:150:1,Ice:150:0.25,Polonium:25:0.1
MinOre=1600000
MaxOre=2500000
Chance=50
SkinType=0
Color=Color(255,255,255)
